cn# Proposal Booster

### Steps to build and run

1. From the project's home directory go to:
  src\main\web-app
   and do: 
  npm install
  
2. Build Angular project:
   npm run build
  
3. From home directory run:
  mvn clean install
 
4. On successful build run the jar:
  java -jar target\srm-0.0.1-SNAPSHOT.jar

5. Then verify the project running on:
  http://localhost:8080/

### Run frontend and backend separately (UI dev mode)

- Run application as usual
- Go to src/main/web-app/
- Execute "ng serve --proxy-config proxy.conf.json" in terminal
- Go to localhost:4200

Client be deployed separately, but will use usual backend.
When some changes done in IDE, angular will automatically rebuild client side and deploy it.