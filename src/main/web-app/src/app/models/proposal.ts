import {Customer} from "./customer";
import {Status} from "./status";
import {Feature} from "../feature/feature";

export class Proposal {
  id : number;
  title: string;
  description: string;
  type: string;

  expected_profit: string;
  man_days: number;
  start_date: string;
  end_date: string;
  equipment: string;
  user_id: string;
  additionalFields: string;
  status: Status;
  customers: Customer[];
  reject_message: string;
  features: Feature[];

  constructor (
  ) {
  }

}
