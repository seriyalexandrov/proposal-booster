export class Status {
  status_id: number;
  name: string;
  description: string;
  public Status (name: string, description: string){
    this.name = name;
    this.description = description;
  }
}
