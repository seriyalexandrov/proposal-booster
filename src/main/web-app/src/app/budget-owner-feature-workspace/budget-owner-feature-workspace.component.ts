import {Component, Input, OnInit} from '@angular/core';
import {Form, FormControl, FormGroup} from "@angular/forms";
import {Proposal} from "../models/proposal";
import {Feature} from "../feature/feature";
import {DataStorage} from "../services/datastorage.service";
import {CustomerService} from "../services/customer.service";
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {UploadService} from "../services/upload.service";
import {EditorService} from "../editor/editor.service";
import {FeatureService} from "../feature/feature.service";
import {Location} from "@angular/common";
import {FeatureStorage} from "../services/featurestorage.service";
import {IdStorage} from "../services/IdStorage";


@Component({
  selector: 'app-budget-owner-feature-workspace',
  templateUrl: './budget-owner-feature-workspace.component.html',
  styleUrls: ['./budget-owner-feature-workspace.component.css']
})
export class BudgetOwnerFeatureWorkspaceComponent implements OnInit {

  feature: Feature = {
    priority: null,
    name: "",
    description: "",
    loe_per_feature: 0,
    feature_id: null,
    proposal_id: null,
    customer_id: null
  }

  features: Feature[];
  role: string;


  constructor(private service: FeatureService,
              private featurestorage: FeatureStorage,
              private router: Router,
              private http: HttpClient,
              private upload: UploadService,
              private location: Location,
              private idstorage: IdStorage) {
    this.features = this.featurestorage.getData();
    if (this.feature === null) {
      console.log("No features")
      this.feature = new Feature();
    }
  }

  selectedFeature: Feature = new Feature();

  ngOnInit() {
    this.http.get('/security/roles').subscribe((data: string[]) => this.role = data[0]);
    /*if (this.idstorage.getId() != null) {
      this.http.get<any>(`/features/${this.idstorage.getId()}`).subscribe((x: Feature []) => this.features = x);
    }*/
  }

  onSelect(one: Feature): void {
    this.selectedFeature = one;
  }

  cancel() {
    this.router.navigate(["/boworkspace"])
  }

}
