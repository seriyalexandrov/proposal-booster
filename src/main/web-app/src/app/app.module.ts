import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { MaterialModules } from './material';
import { HttpClientModule }    from '@angular/common/http';

import 'hammerjs';
import {ListOfProposalsComponent} from "./list-of-proposals/list-of-proposals.component";
import {ProposalDetailComponent} from "./proposal-detail/proposal-detail.component";
import {EditorComponent} from "./editor/editor.component";

import {HttpModule} from "@angular/http";
import {DataStorage} from "./services/datastorage.service";
import { BudgetOwnerWorkspaceComponent } from './budget-owner-workspace/budget-owner-workspace.component';
import { BudgetOwnerFeatureWorkspaceComponent } from './budget-owner-feature-workspace/budget-owner-feature-workspace.component';
import {FeatureComponent} from "./feature/feature.component";
import {IdStorage} from "./services/IdStorage";
import {FeatureStorage} from "./services/featurestorage.service";


@NgModule({
  declarations: [
    AppComponent,
    ListOfProposalsComponent,
    ProposalDetailComponent,
    EditorComponent,
    FeatureComponent,
    BudgetOwnerWorkspaceComponent,
    BudgetOwnerFeatureWorkspaceComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    FormsModule,
    MaterialModules,
    ReactiveFormsModule,
    HttpClientModule,
    HttpModule
  ],
  entryComponents:[],
  providers: [DataStorage, IdStorage, FeatureStorage],
  bootstrap: [AppComponent]
})
export class AppModule { }
