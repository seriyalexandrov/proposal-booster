import {Component, Input, OnInit} from '@angular/core';
import {Proposal} from '../models/proposal';

@Component({
  selector: 'app-proposal-detail',
  templateUrl: './proposal-detail.component.html',
  styleUrls: ['./proposal-detail.component.css']
})
export class ProposalDetailComponent implements OnInit {

  constructor() { }

  @Input() selectedProposal: Proposal;

  ngOnInit() {
  }

}
