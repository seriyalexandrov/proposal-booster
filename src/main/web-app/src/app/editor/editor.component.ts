import {Component, Input, OnInit} from '@angular/core';
import {Form, FormControl, FormGroup} from "@angular/forms";
import {EditorService} from "./editor.service";
import {Proposal} from "../models/proposal";
import {DataStorage} from "../services/datastorage.service";
import {Customer} from "../models/customer";
import {CustomerService} from "../services/customer.service";
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {UploadService} from "../services/upload.service";
import {Status} from "../models/status";
import {Feature} from "../feature/feature";
import {e} from "@angular/core/src/render3";
import {IdStorage} from "../services/IdStorage";
import {forEach} from "@angular/router/src/utils/collection";
import {FeatureStorage} from "../services/featurestorage.service";


@Component({
  selector: 'app-root',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css'],
  providers: [EditorService]
})
export class EditorComponent implements OnInit {

  Check: boolean = true;
  message_status = "hidden";
  messagesave = '';
  messagedraftsave = '';
  messagecheck = '';
  message = '';
  customersList: Customer[] = [];
  // selectedCustomers: Customer[] = [];
  featureList: Feature[] = [];
  selectedCustomers: Customer[] = [];
  newCustomer: string = '';
  isFeatureButtonClicked: boolean = false;
  selectedFile: File;

  proposal: Proposal = new Proposal();
  role: string;

  /*proposal: Proposal = {
    id: null,
    title: "",
    description: "",
    type: "",
    expected_profit: 0,
    man_days: 0,
    start_date: null,
    end_date: null,
    equipment: "",
    user_id: null,
    additionalFields: "",
    status: null,
    customers: [],
    reject_message: "",
  };*/

  constructor(private service: EditorService,
              private storage: DataStorage,
              private idstorage: IdStorage,
              private customer: CustomerService,
              private router: Router,
              private http: HttpClient,
              private upload: UploadService,
              private featurestorage: FeatureStorage) {
    this.proposal = this.storage.getData();
    this.featureList = this.featurestorage.getData();
    if (this.proposal == null) {
      this.proposal = new Proposal();
    }
  }

  ngOnInit() {
    this.service.getCustomers().subscribe(customers => {
      console.log(customers);
      this.customersList = customers
    });
    if (this.idstorage.getId() != null) {
      this.http.get<any>(`/proposals/${this.idstorage.getId()}`).subscribe((x: Proposal) => {
          this.proposal = x;
          console.log("da");
          this.http.get<any>(`/customers/${this.idstorage.getId()}`).subscribe((x: Customer[]) => {
            console.log("Selected:::: " + x);
            this.selectedCustomers = x;
            console.log(x.valueOf());
            if (typeof this.selectedCustomers === 'undefined' || this.selectedCustomers.length <= 0) {
              this.selectedCustomers = [];
              console.log("bad")
            }
          });

        }
      );
    }
  }

  compareObjects(o1: Customer, o2: Customer): boolean {
    return o1.customer_id === o2.customer_id && o1.name === o2.name;
  }

  saveAsDraft() {
    if (this.idstorage.getId() == null) {
      let stat = this.proposal.type;
      let tempStat = new Status();
      this.proposal.status = tempStat;
      this.proposal.status.name = "In Work";
      this.proposal.status.description = "In Work";
      this.proposal.customers = this.selectedCustomers;
      // this.proposal.customers = this.customersList.splice(0, this.customersList.length+1);
      this.service.createProposal(this.proposal, stat, [this]);
    } else {
      /*let stat = this.proposal.type;
      let tempStat = new Status();
      this.proposal.status = tempStat;
      this.proposal.status.name = "Initiated";
      this.proposal.status.description = "Initiated";*/
      this.proposal.customers = this.selectedCustomers;
      // this.proposal.customers = this.customersList.splice(0, this.customersList.length+1);
      this.service.updateProposal(this.proposal, 'In Work', this.selectedCustomers, [this]);
      this.featurestorage.setData(null);
      this.router.navigate(['/']);
    }
  }

  submit() {
    if (this.idstorage.getId() == null) {
      let stat = this.proposal.type;
      let tempStat = new Status();
      this.proposal.status = tempStat;
      this.proposal.status.name = "In Queue";
      this.proposal.status.description = "In Queue";
      this.proposal.customers = this.selectedCustomers;
      // this.proposal.customers = this.customersList.splice(0, this.customersList.length+1);
      this.service.createProposal(this.proposal, stat, [this]);
    } else {
      this.proposal.customers = this.selectedCustomers;
      // this.proposal.customers = this.customersList.splice(0, this.customersList.length+1);
      this.service.updateProposal(this.proposal, 'In Queue', this.selectedCustomers, [this]);
      this.featurestorage.setData(null);
      this.router.navigate(['/']);
    }
  }

  validateProposal() {
    this.Check = false;
    this.messagecheck = 'you proposal has been send to check';
    if (this.service.validateProposal(this.proposal, this.featureList)) {
      console.log("VALID!!!!");
      this.message = "Correct";
      this.message_status = "alert alert-success in";
    } else {
      console.log("INVALID!!!");
      this.message = "Wrong checksum. Please check LOE";
      this.message_status = "alert alert-danger in";
    }
  }

  addNewCustomer() {
    if (this.newCustomer) {
      this
        .customer
        .postCustomer(this.newCustomer)
        .subscribe(
          result => this.customersList.push(result)
        );
      this.newCustomer = "";
    }
  }

  cancel() {
    this.idstorage.setId(null);
    this.featurestorage.setData(null);
    this.router.navigate(['/']);
  }

  feature() {
    this.featurestorage.setData(this.featureList);
    this.storage.setData(this.proposal);
    this.idstorage.setId(this.proposal.id);
    this.router.navigate(['featurepage']);
  }

  featureButtonClicked() {
    this.isFeatureButtonClicked = true;
  }

  selectFile(event) {
    this.selectedFile = <File>event.target.files[0];
  }

  uploadFile() {
    this.upload.uploadFile('/upload/post', this.selectedFile, this.proposal.title)
      .subscribe(e => { //поставить на следующую строчку баг и посмотреть что возвращает е
          console.log(e);// - е это список фич, его надо из json преобразовать в массив и добавить к пропозалу
          //alert(e);
          //this.featureList.push(e);
          this.featureList = e as Feature[];
          console.log(this.featureList);
        }
      )

    // console.log("d");
    // console.log(this.featureList);
  }
}
