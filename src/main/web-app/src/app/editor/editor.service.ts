import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Proposal} from "../models/proposal";
import {Observable} from 'rxjs';
import {Customer} from "../models/customer";
import {Router} from "@angular/router";
import {Feature} from "../feature/feature";
import {forEach} from "@angular/router/src/utils/collection";
import {EditorComponent} from "./editor.component";
import {IdStorage} from "../services/IdStorage";

@Injectable({
  providedIn: 'root',
})

export class EditorService {

  private customer: Customer;
  constructor(private router: Router,
              private idstorage: IdStorage,
              private http: HttpClient) {}

  createProposal(proposal1: Proposal, stat: string, messages: EditorComponent[]): boolean {
    let new_status = proposal1.status.name;
    let old_status = "not created";
    if (stat != null) {
      old_status = stat;
    }
    let id = null;
    let isOk = true;
    proposal1.status = null;
    this.http.post('/proposals', {proposal:proposal1, customers:proposal1.customers,
      oldStatus:old_status, newStatus:new_status, featureList: messages[0].featureList}).subscribe(
      x => {
        console.log("inform" + x);
        isOk = true;
        this.idstorage.setId(null);
        this.router.navigate(['/']);
      }
        , e => {
        console.log("error: " + e);

        messages[0].message = 'Error occured!';
        messages[0].message_status = 'alert alert-danger in';
       // this.http.post('/proposals/{id}',{id,proposal:proposal1, } )
        isOk = false;
      }
    );

    return isOk;
   // this.http.post('/proposals/{id}',{id,proposal:proposal1,} )

  }

  updateProposal(proposal: Proposal, stat: string, customers: Customer[], messages: EditorComponent[]): void {
    this.http.put(`/proposals/${proposal.id.toString()}`, {proposal: proposal, newStatus: stat,
      customers: customers, featureList: messages[0].featureList}).subscribe(
      x => {
        console.log(x);
        this.router.navigate(['/']);
      },
      e => console.log(e)
    );
  }

  getCustomers(): Observable<any> {
    return this.http.get('/customers')
  }

  validateProposal(proposal: Proposal, featureList: Feature[]): boolean {

    let sum_features = 0;

    featureList.forEach(function (value, index, array) {
      sum_features += value.loe_per_feature;
    });

   return proposal.man_days == sum_features;
    // this.http.post('/validateProposal', proposal).subscribe(
    //   x => console.log("ok: " + x),
    //   e => console.log("error: " + e)
    // );
  }


}
