import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { TestObjectData } from '../models/test-object-data';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  private requestsUrl = '/api/testobjects/';

  constructor(private http: HttpClient) { }

  getRequests(): Observable<TestObjectData[]> {
    return this.http.get<TestObjectData[]>(this.requestsUrl)
    .pipe(
      catchError(this.handleError('getRequest', []))
    );
  }


  /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
private handleError<T> (operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {
    console.error(error); // log to console instead
    // Let the app keep running by returning an empty result.
    return of(result as T);
  };
}

}
