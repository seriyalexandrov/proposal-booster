import { Injectable } from '@angular/core';
import {HttpClient, HttpParams, HttpRequest, HttpEvent, HttpResponse} from '@angular/common/http';
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  constructor(private http: HttpClient) { }

  uploadFile(url: string, file: File, title: string): Observable<any> {

    let formData = new FormData();
    formData.append('file', file, file.name);
    formData.append('proposalName', title);

    let params = new HttpParams();

    const options = {
      params: params,
      reportProgress: true,
    };

   // const req = new HttpRequest('POST', url, formData, options);
    return this.http.post(url, formData, options);
  }
}
