import { Injectable } from '@angular/core';

@Injectable()
export class DataStorage {
  private data: any;
  public constructor() { }
  public setData(data: any): void {
    this.data = data;
  }
  public getData(): any {
    return this.data;
  }
}
