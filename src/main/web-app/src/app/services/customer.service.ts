import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Customer} from "../models/customer";
import {Observable} from "rxjs/index";

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  constructor (private http: HttpClient){}

  postCustomer(newCustomer: string): Observable<any>  {
    return this.http.post('/customers', new Customer(null, newCustomer));
  }
}
