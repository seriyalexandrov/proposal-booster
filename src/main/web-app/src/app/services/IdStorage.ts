import { Injectable } from '@angular/core';

@Injectable()
export class IdStorage {
  private id: number;
  public constructor() { }
  public setId(data: any): void {
    this.id = data;
  }
  public getId(): any {
    return this.id;
  }
}
