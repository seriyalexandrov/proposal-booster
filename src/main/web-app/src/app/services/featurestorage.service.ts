import { Injectable } from '@angular/core';
import {Feature} from "../feature/feature";

@Injectable()
export class FeatureStorage {
  private data: Feature[];
  public constructor() { }
  public setData(data: Feature[]): void {
    this.data = data;
  }
  public getData(): Feature[] {
    return this.data;
  }
}
