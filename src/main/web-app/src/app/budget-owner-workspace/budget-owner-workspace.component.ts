import {Component, Input, OnInit} from '@angular/core';
import {Proposal} from "../models/proposal";
import {Route, Router} from "@angular/router";
import {DataStorage} from "../services/datastorage.service";
import {Customer} from "../models/customer";
import {EditorService} from "../editor/editor.service";
import {Observable} from "rxjs";
import {BoworkspaceService} from "./boworkspace.service";
import {AppRoutingModule} from "../app-routing.module";
import {HttpClient} from "@angular/common/http";
import {IdStorage} from "../services/IdStorage";
import {Feature} from "../feature/feature";
import {FeatureStorage} from "../services/featurestorage.service";

@Component({
  selector: 'app-budget-owner-workspace',
  templateUrl: './budget-owner-workspace.component.html',
  styleUrls: ['./budget-owner-workspace.component.css']
})
export class BudgetOwnerWorkspaceComponent implements OnInit {


  constructor(private router: Router,
              private idstorage: IdStorage,
              private service: BoworkspaceService,
              private http: HttpClient,
              private featurestorage: FeatureStorage) {
    this.features = this.featurestorage.getData();
  }

  selectedProposal: Proposal = new Proposal();
  customerList: Customer[];
  role: string;
  features: Feature [];

  /*selectedProposal: Proposal = this.service.getProposal();*/

  public is_rejected: boolean = false;
  public statusName: string;
  public length: number;

  ngOnInit() {
    /*this.selectedProposal = this.storage.getData();
    this.statusName = this.selectedProposal.status.name;
    console.log("status Name" + this.statusName);*/
    this.http.get<any>(`/proposals/${this.idstorage.getId()}`).subscribe((x: Proposal) => {
      this.selectedProposal = x;
      this.statusName = this.selectedProposal.status.name
    });
    this.http.get<any[]>(`/customers/${this.idstorage.getId()}`).subscribe((x: Customer[]) => {
      this.customerList = x;
      this.length = this.customerList.length
    });
    this.http.get('/security/roles').subscribe((data: string[]) => this.role = data[0]);
    if (this.idstorage.getId() != null) {
      this.http.get<any>(`/features/${this.idstorage.getId()}`).subscribe((x: Feature []) => this.features = x);
    }

  }

  changeStatus() {

  }

  approve() {
    this.service.changeStatus(this.selectedProposal, 'Approved', false);
    this.idstorage.setId(null);
    this.router.navigate(['/']);
  }

  showRejectionMessage() {
    this.is_rejected = true;
  }

  reject() {
    this.service.changeStatus(this.selectedProposal, 'In Work', true);
    this.idstorage.setId(null);
    this.router.navigate(['/']);
  }

  cancel() {
    this.idstorage.setId(null);
    this.router.navigate(['/']);
  }

  featurePage(){
    this.featurestorage.setData(this.features);
    this.router.navigate(['boworkfeaturespace']);
  }

}
