import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Proposal} from "../models/proposal";
import {Observable} from 'rxjs';
import {Customer} from "../models/customer";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root',
})

export class BoworkspaceService {

  constructor(private router: Router,
              private http: HttpClient) {}

  changeStatus (proposal: Proposal, stat: string, isMessage: boolean): void {
    this.http.put(`/proposalUpdate/` +  proposal.id.toString(), {proposal: proposal, newStatus: stat, isMessage: isMessage}).subscribe(
      x => {
        console.log(x);
        this.router.navigate(['/']);
      },
      e => console.log(e)
    );
  }

}
