import {Proposal} from "../models/proposal";
import {Customer} from "../models/customer";

export class Feature{
  priority: number;
  name: string;
  description: string;
  loe_per_feature: number;
  feature_id: number;
  proposal_id: Proposal;
  customer_id: Customer;
  constructor (
  ) {
  }
}
