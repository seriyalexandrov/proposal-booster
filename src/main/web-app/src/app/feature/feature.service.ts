import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Router} from "@angular/router";
import {Feature} from "./feature";

@Injectable({
  providedIn: 'root',
})

export class FeatureService {

  constructor(private router: Router,
              private http: HttpClient) {};

  createfeature(feature1: Feature): void {
    this
      .http
      .post('/feature', {feature: feature1})
      .subscribe(result => console.log("Feature added " + result));
  }


}
