import {Component, Input, OnInit} from '@angular/core';
import {Form, FormControl, FormGroup} from "@angular/forms";
import {Proposal} from "../models/proposal";
import {Feature} from "../feature/feature";
import {DataStorage} from "../services/datastorage.service";
import {CustomerService} from "../services/customer.service";
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {UploadService} from "../services/upload.service";
import {EditorService} from "../editor/editor.service";
import {FeatureService} from "../feature/feature.service";
import {Location} from "@angular/common";
import {FeatureStorage} from "../services/featurestorage.service";
import {IdStorage} from "../services/IdStorage";


@Component({
  selector: 'app-root',
  templateUrl: './feature.component.html',
  styleUrls: ['./feature.component.css'],
  providers: [FeatureService]
})
export class FeatureComponent implements OnInit {

  message = '';

  feature: Feature = {
    priority: null,
    name: "",
    description: "",
    loe_per_feature: 0,
    feature_id: null,
    proposal_id: null,
    customer_id: null
  }

  features: Feature[];
  role: string;
//cfeatureListL: Feature[];

  constructor(private service: FeatureService,
              private featurestorage: FeatureStorage,
              private router: Router,
              private http: HttpClient,
              private upload: UploadService,
              private location: Location,
              private idstorage: IdStorage) {
    this.features = this.featurestorage.getData();
    /*if ((this.role == 'admin') || (this.role == 'budgetOwner'))
      this.features = this.featurestorage.getData();
    else 
      this.features = new Array<Feature>();*/
    if (this.feature === null) {
      console.log("No features")
      this.feature = new Feature();
    }
  }

  selectedFeature: Feature = new Feature();


  ngOnInit() {
    this.http.get('/security/roles').subscribe((data: string[]) => this.role = data[0]);
    /*if (this.idstorage.getId() != null) {
      this.http.get<any>(`/features/${this.idstorage.getId()}`).subscribe((x: Feature []) => this.features = x);
    }*/
  }

  onSelect(one: Feature): void {
    this.selectedFeature = one;
  }
  newfeature: Feature = new Feature();
  featureList: Feature [] = [];


  addFeature() {
    let buf = new Feature();
    this.features.push(this.newfeature);
    this.featureList.push(this.newfeature);
    this.newfeature = buf;
  }

  save(){
    this.featurestorage.setData(this.features);
    this.router.navigate(["/details"])
  }
  deleteFeature(){
    this.features.splice(this.features.length-1, 1);
    this.featurestorage.setData(this.features);
  }


  cancel() {
    this.featurestorage.setData(this.features);
    this.router.navigate(["/details"])
  }

}
