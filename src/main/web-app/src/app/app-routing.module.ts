import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {ListOfProposalsComponent} from './list-of-proposals/list-of-proposals.component';
import {EditorComponent} from "./editor/editor.component";
import {BudgetOwnerWorkspaceComponent} from "./budget-owner-workspace/budget-owner-workspace.component";
import {FeatureComponent} from "./feature/feature.component";
import {BudgetOwnerFeatureWorkspaceComponent} from "./budget-owner-feature-workspace/budget-owner-feature-workspace.component";

const appRoutes: Routes = [
  { path: '', component: ListOfProposalsComponent, runGuardsAndResolvers: 'always'},
  { path: 'details', component: EditorComponent},
  {path: 'boworkspace', component: BudgetOwnerWorkspaceComponent},
  {path: 'boworkfeaturespace', component: BudgetOwnerFeatureWorkspaceComponent},
  {path: 'featurepage', component: FeatureComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {onSameUrlNavigation: 'reload'}),
    CommonModule
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
