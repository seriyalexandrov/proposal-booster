import {Component, OnInit} from '@angular/core';
import {Proposal} from '../models/proposal';
import {NavigationEnd, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs/index";
import {map} from "rxjs/internal/operators";
import {EditorService} from "../editor/editor.service";
import {DataStorage} from "../services/datastorage.service";
import {Element} from "@angular/compiler";
import {IdStorage} from "../services/IdStorage";


@Component({
  selector: 'app-list-of-proposals',
  templateUrl: './list-of-proposals.component.html',
  styleUrls: ['./list-of-proposals.component.css'],
  providers: []
})
export class ListOfProposalsComponent implements OnInit {


  constructor(
    private router: Router,
    private http: HttpClient,
    private storage: DataStorage,
    private idstorage: IdStorage
  ) {
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
        this.initialiseInvites();
      }
    });
  }

  initialiseInvites() {
    this.getAllProposals().pipe(map((Proposal1: Array<any>) => {
      let result: Array<Proposal> = [];
      if (Proposal1) {
        Proposal1.forEach((erg) => {
          let prep = new Proposal();
          prep.id = erg.id;
          prep.title = erg.title;
          prep.description = erg.description;
          prep.equipment = erg.equipment;
          prep.start_date = erg.start_date;
          prep.end_date = erg.end_date;
          prep.expected_profit = erg.expected_profit;
          prep.man_days = erg.man_days;
          prep.status = erg.status;
          prep.type = erg.status ? erg.status.name : null;
          result.push(prep);
        });
      }
      return result;
    }))
      .subscribe(Proposal => this.allProposals = Proposal);

    this.getAllStatuses().pipe(map((Element: Array<any>) => {
      let result: Array<String> = [];
      if (Element) {
        Element.forEach((erg) => {
          result.push(erg.name);
        });
      }
      return result; // <<<=== missing return
    }))
      .subscribe(Element => this.typeOfProposals = Element);

    this.http.get('/security/roles').subscribe((data: string[]) => this.role = data[0]);
    console.info("this.role"+this.role);
  }


  getAllProposals(): Observable<any[]> {
    return this.http.get<any[]>("/getProposals");
  }

  getAllStatuses(): Observable<any[]> {
    return this.http.get<any[]>("/statuses");
  }

  Idproposal: number;

  typeOfProposals: String[];

  typeOfProposal: string = 'All'; //

  selectedProposal: Proposal;
  allProposals: Proposal[] = [];
  role: string = '';
  navigationSubscription;


  changeType(type: string): void {
    this.typeOfProposal = type;
  }

  onSelect(one: Proposal): void {
    this.selectedProposal = one;
  }

  createNew() {
    this.router.navigate(['/details']);
  }

  editProposal(one: Proposal): void {
    this.idstorage.setId(one.id);
    if ((this.role == 'admin') || (this.role == 'budgetOwner')) this.router.navigate(['/boworkspace']);
    else this.router.navigate(['/details']);
  }

  downloadProposal(proposal: Proposal): void {
    this.http.get('/upload/pptx?id=' + proposal.id).subscribe(data => {
      const blob = new Blob([data], {type: 'text/csv'});
      const url = window.URL.createObjectURL(blob);
      window.open(url);
    });
  }

  getAllProposalsByUserId(): Observable<any[]>{
    return this.http.get<any[]>("");
  }

  /*getUserIdByLogin(log: String): any{ // как отправить user_login
    log = this.userLogin;
    return this.http.get("userId");
  }

  getUserLogin(): any{
    return this.http.get("userLogin");
  }*/

  ngOnInit() {
    //this.role = 'budgetOwner';
   // if ((this.role == 'admin') || (this.role == 'budgetOwner')) {
      this.getAllProposals().pipe(map((Proposal1: Array<any>) => {
        let result: Array<Proposal> = [];
        if (Proposal1) {
          Proposal1.forEach((erg) => {
            let prep = new Proposal();
            prep.id = erg.id;
            prep.title = erg.title;
            prep.description = erg.description;
            prep.equipment = erg.equipment;
            prep.start_date = erg.start_date;
            prep.end_date = erg.end_date;
            prep.expected_profit = erg.expected_profit;
            prep.man_days = erg.man_days;
            prep.status = erg.status;
            prep.type = erg.status ? erg.status.name : null;
            result.push(prep);
          });
        }
        return result;
      }))
        .subscribe(Proposal => this.allProposals = Proposal);

      this.getAllStatuses().pipe(map((Element: Array<any>) => {
        let result: Array<String> = [];
        if (Element) {
          Element.forEach((erg) => {
            result.push(erg.name);
          });
        }
        return result; // <<<=== missing return
      }))
        .subscribe(Element => this.typeOfProposals = Element);
      this.http.get('/security/roles').subscribe((data: string[]) => this.role = data[0]);
    /*} else {
      //this.userLogin = this.getUserLogin();
      //this.userId = this.getUserIdByLogin(this.login);
      this.getAllProposalsByUserId();
    }*/
  }



  ngOnDestroy() {
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }


}
