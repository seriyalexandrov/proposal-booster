package edu.proposalbooster.security;

import edu.proposalbooster.database.model.Users.Users;
import edu.proposalbooster.database.service.Users.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class SecurityService implements UserDetailsService {
    @Autowired
    UsersService users;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDetails loadedUser;
        try {
            Users client = users.getUserByLogin(username);
            loadedUser = new User(client.getLogin(), client.getPassword(), getAuthorities(client.getRole()));
        }
        catch (Exception repositoryProblem) {
            throw new InternalAuthenticationServiceException("Bad credentials");
        }
        return loadedUser;
    }

    private Collection<? extends GrantedAuthority> getAuthorities(String role) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(role));
        return authorities;
    }
}
