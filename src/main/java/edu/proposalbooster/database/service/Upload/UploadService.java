package edu.proposalbooster.database.service.Upload;

import edu.proposalbooster.database.service.Features.FeatureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class UploadService {
    private final String pptx = ".pptx";
    private final String xlsx = ".xlsx";

    @Autowired
    FeatureService featureService;

    public String storeFile(MultipartFile file) {
        try {
            Path path = getRoot().resolve(file.getOriginalFilename());
            File fileName = new File(path.toUri());
            if (fileName.exists()) {
                fileName.delete();
            }
            Files.copy(file.getInputStream(), path);
            return fileName.toString();
        } catch (Exception e) {
            throw new RuntimeException("Error");
        }
    }

    public Resource loadFile(String fileName) {
        try {
            Path file = getRoot().resolve(fileName);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("Error");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error");
        }
    }

    public Resource loadFile(Path file) {
        try {
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("Error");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error");
        }
    }

    public Path getRoot() {
        return Paths.get(this.getClass().getClassLoader().getResource("/store").toString());
    }

    public String getPPTXNameForFile(String proposalName) {
        return proposalName + pptx;
    }

    public String getXLSXNameForFile(String proposalName) {
        return proposalName + xlsx;
    }
}
