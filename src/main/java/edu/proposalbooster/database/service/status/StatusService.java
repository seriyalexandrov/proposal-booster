package edu.proposalbooster.database.service.status;

import edu.proposalbooster.database.model.Statuses.Statuses;
import edu.proposalbooster.database.repository.Statuses.StatusChangesRepository;
import edu.proposalbooster.database.repository.Statuses.StatusesRepository;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StatusService {

    @Autowired
    private StatusesRepository statusesRepository;

    @Autowired
    private StatusChangesRepository statusChangesRepository;
//
//    private Graph graph;
//
//    @PostConstruct
//    public void init() {
//        List<Long> nodes =  statusesRepository.findAll()
//                .stream()
//                .map(Statuses::getStatus_id)
//                .collect(Collectors.toList());
//        List<Pair<Long, Long>> edges = statusChangesRepository.findAll()
//                .stream()
//                .map(statusChanges -> new Pair<>(statusChanges.getStatus_id(), statusChanges.getStatus2_id()))
//                .collect(Collectors.toList());
//        this.graph = new Graph(nodes.size());
//        for (Pair<Long, Long> edge : edges) {
//            this.graph.addEdge(edge.getKey(), edge.getValue());
//        }
//    }

    public Statuses getStatusesByName(String newStatus) {
        return statusesRepository.getStatusesByName(newStatus);
    }

//    private class Graph{
//        private List<List<Long>> adj;
//
//        Graph(int size) {
//            adj = new ArrayList<>();
//            for(int i = 0; i < size; ++i) {
//                adj.add(new ArrayList<>());
//            }
//        }
//
//        void addEdge(Long a, Long b) {
//            adj.get(a.intValue()).add(b);
//        }
//
//        boolean isEdgeExist(Long a, Long b) {
//            return adj.get(a.intValue()).contains(b);
//        }
//    }

    public boolean isValid(Statuses oldStatus, Statuses newStatus) {
        Long a = oldStatus.getStatus_id();
        Long b = newStatus.getStatus_id();
        System.out.println(String.valueOf(a) + " " + String.valueOf(b));
        return statusChangesRepository.findByStatus1IdAndStatus2Id(a, b).isPresent();
    }

    public List<Statuses> getAll() {
        return statusesRepository.findAll();
    }
}