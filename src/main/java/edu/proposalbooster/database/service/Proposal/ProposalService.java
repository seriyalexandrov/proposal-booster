package edu.proposalbooster.database.service.Proposal;

import com.google.gson.Gson;
import edu.proposalbooster.database.model.Features.Features;
import edu.proposalbooster.database.model.ProposalCustomer.ProposalCustomer;
import edu.proposalbooster.database.model.Proposals.AddtionalFields.AdditionalFields;
import edu.proposalbooster.database.model.Proposals.Proposals;
import edu.proposalbooster.database.model.Customers.Customers;
import edu.proposalbooster.database.model.Statuses.Statuses;
import edu.proposalbooster.database.model.Users.Users;
import edu.proposalbooster.database.repository.Proposals.ProposalsRepository;
import edu.proposalbooster.database.repository.ProposalCustomer.ProposalCustomerRepository;
import edu.proposalbooster.database.service.EmailSender.EmailSenderService;
import edu.proposalbooster.database.service.Features.FeatureService;
import edu.proposalbooster.database.service.Users.UsersService;
import edu.proposalbooster.database.service.status.StatusService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class ProposalService {

    @Autowired
    private ProposalsRepository proposalsRepository;
    @Autowired
    private ProposalCustomerRepository proposalCustomerRepository;
    @Autowired
    private StatusService statusService;
    @Autowired
    private EmailSenderService emailSenderService;
    @Autowired
    private UsersService usersService;
    @Autowired
    private FeatureService featureService;

    @PersistenceContext
    private EntityManager entityManager;

    public List<Proposals> getByUser_id(String userId) {
        Query query = entityManager.createNativeQuery(
                "SELECT p.* FROM proposals p WHERE p.user_id = :userID");
        query.setParameter("userID", new BigInteger(userId));
        List<Object[]> result = query.getResultList();
        List<Proposals> convertedResults = new ArrayList<>(result.size());
        for(Object[] row : result) {
            convertedResults.add(new Proposals ((BigInteger)row[0],
                                                (AdditionalFields[])row[1],
                                                (String)row[2],
                                                (String)row[3],
                                                (String)row[4],
                                                (String)row[5],
                                                (BigInteger)row[6],
                                                (String)row[7],
                                                (String)row[8],
                                                (String)row[9],
                                                (BigInteger)row[10],
                                                (BigInteger) row[11] ));
        }
        return convertedResults;
    }

    public Proposals changeStatusOfProposal(Long proposalId, JSONObject jsonBody, String status, boolean isMessage) throws Exception {
        Gson g = new Gson();
        System.out.println("Parse");
        Proposals tempProposal = g.fromJson(jsonBody.toString(), Proposals.class);
        Proposals proposal = proposalsRepository.findById(proposalId).get();
        System.out.println("find");
        Statuses newStatus = statusService.getStatusesByName(status);
        Statuses oldStatus = proposal.getStatus();


        if (statusService.isValid(oldStatus, newStatus)) {
            System.out.println("IsValid");
            //sendEmailMessage(tempProposal.getUser_id(), oldStatus.getName(), newStatus.getName());
            proposal.setStatus(newStatus);
        } else {
            System.out.println("IsInValid");
            throw new Exception(oldStatus+"->"+newStatus);
        }

        if (isMessage) {
            proposal.setReject_message(tempProposal.getReject_message());
        }

        proposal = proposalsRepository.saveAndFlush(proposal);
        System.out.println("Mer");
        sendEmailMessage(proposal.getUser_id(), oldStatus.toString(), status, isMessage ? tempProposal.getReject_message() : "", proposal);
        System.out.println("Her");
        return proposal;
    }

    public Long updateProposal(Long proposalId, JSONObject jsonBody, String status, JSONArray customersArr) throws Exception{
        Gson g = new Gson();
        Proposals tempProposal = g.fromJson(jsonBody.toString(), Proposals.class);
        Proposals proposal = proposalsRepository.findById(proposalId).get();

        Statuses newStatus = statusService.getStatusesByName(status);
        Statuses oldStatus = proposal.getStatus();

        if (statusService.isValid(oldStatus, newStatus)) {

            //sendEmailMessage(tempProposal.getUser_id(), oldStatus.getName(), newStatus.getName());
            proposal.setStatus(newStatus);
        } else {
            throw new Exception(oldStatus+"->"+newStatus);
        }

        proposal.setAdditionalFields(tempProposal.getAdditionalFields());
        proposal.setTitle(tempProposal.getTitle());
        proposal.setDescription(tempProposal.getDescription());
        proposal.setExpected_profit(tempProposal.getExpected_profit());
        proposal.setStart_date(tempProposal.getStart_date());
        proposal.setEnd_date(tempProposal.getEnd_date());
        proposal.setEquipment(tempProposal.getEquipment());
        proposal.setUser_id(tempProposal.getUser_id());
        proposal.setMan_days(tempProposal.getMan_days());

        proposal.setReject_message(tempProposal.getReject_message());

        proposal = proposalsRepository.saveAndFlush(proposal);
        return proposalId;
    }

    private void sendEmailMessage(Users user, String oldStatus, String newStatus, String message, Proposals proposal) {
        try {
            String body = "Proposal with title " + proposal.getTitle() + " had status of <i>" +
                    oldStatus + "</i> new status is <i>" + newStatus + "</i> <br>";
            if (message != null && !message.isEmpty()) {
                body = body + "Message: <span style='color:red'>" + message + "</span>";
            }
            emailSenderService.sendEmail(user.getEmail(), body, "Title : " + proposal.getTitle() +
                    " new status is " + newStatus);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Long createProposal(JSONObject jsproposal,  String oldStatus, String newStatus) throws Exception {
        Gson g = new Gson();
        Proposals proposal = g.fromJson(jsproposal.toString(), Proposals.class);
        Users user = usersService.getUserByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
        proposal.setUser_id(user);
        Statuses statuses = statusService.getStatusesByName(newStatus);
        proposal.setStatus(statuses);
        System.out.println(proposal.getTitle() + String.valueOf(proposal.getId()));
        proposal = proposalsRepository.saveAndFlush(proposal);
        Long id = proposal.getId();
        /*JSONArray customersArr = jsproposal.getJSONArray("customers");
        JSONObject customerJ;
        for (int i=0; i < customersArr.length(); i++) {
            customerJ = customersArr.getJSONObject(i);
            Customers customer = g.fromJson(customerJ.toString(), Customers.class);
            ProposalCustomer proposalCustomer = new ProposalCustomer();
            proposalCustomer.setCustomer_id(customer);
            proposalCustomer.setProposal_id(proposal);
            proposalCustomer.setCustomerPriority(1);
            proposalCustomerRepository.save(proposalCustomer);
        }*/

        sendEmailMessage(proposal.getUser_id(), oldStatus, newStatus, "", proposal);
        sendEmailMessage(usersService.getUserByRole("budgetOwner"), oldStatus, newStatus, "", proposal);


        return id;
    }

    public List<Proposals> getAll() {
        return proposalsRepository.findAll();
    }

    public List<Statuses> getAllStatuses() {
        return statusService.getAll();
    }

    public Proposals findById(Long id){
        return proposalsRepository.findById(id).get();
    }

    //public Long getProposalId(){ return proposal}

    public void addFeature(Features feature) {
        featureService.save(feature);
    }

    public void addFeature(Long proposalId, String name, int priority, Customers customers, double loe, String description) {
        Proposals proposals = proposalsRepository.findById(proposalId).get();
        featureService.save(new Features(name, priority, proposals, loe, description, customers));
    }

    public List<Features> getFeaturesByProposalId(Long proposalId) {
//        Proposals proposals = proposalsRepository.findById(proposalId).get();
        return featureService.getAllByProposalId(proposalId);
    }

    public void saveProposal(Proposals proposals) {
        proposalsRepository.saveAndFlush(proposals);
    }

}