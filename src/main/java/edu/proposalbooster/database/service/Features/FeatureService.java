package edu.proposalbooster.database.service.Features;

import com.google.gson.Gson;
import edu.proposalbooster.database.model.Customers.Customers;
import edu.proposalbooster.database.model.Features.Features;
import edu.proposalbooster.database.model.Proposals.Proposals;
import edu.proposalbooster.database.repository.Features.FeaturesRepository;
import edu.proposalbooster.database.service.Customers.CustomersService;
import edu.proposalbooster.database.service.Proposal.ProposalService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class FeatureService {
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    public FeaturesRepository featuresRepository;
    @Autowired
    public ProposalService proposalService;
    @Autowired
    public CustomersService customersService;

    public List<Features> getAll() {
        return featuresRepository.findAll();
    }

    public Optional<Features> findById(Long id) {
        return featuresRepository.findById(id);
    }

    public Optional<Features> findByName(String name) {
        return featuresRepository.findByName(name);
    }

    public List<Features> getAllByProposalId(Long proposalId) {
        Query query = entityManager.createNativeQuery(
                "select feature_id, name, priority, customer_id, loe_per_feature, description from features where proposal_id = ?1");
        query.setParameter(1, new BigInteger(Long.toString(proposalId)));
        List<Object[]> result = query.getResultList();
        List<Features> convertedResults = new ArrayList<>(result.size());
        for(Object[] row : result) {
            Proposals proposals = proposalService.findById(proposalId);
            Customers customers = customersService.findById(((BigInteger)row[3]).longValue());
            convertedResults.add(new Features((BigInteger) row[0], (String) row[1], (Integer) row[2],
                    proposals, (double)row[4], (String)row[5], customers));
        }
        return convertedResults;
    }

    public Features save(Features entity) {
        return featuresRepository.saveAndFlush(entity);
    }

    public void createFeature(JSONObject jsfeature, Long idProposal){
        Gson g = new Gson();
        Features feature = g.fromJson(jsfeature.toString(), Features.class);
        feature.setProposal_id(proposalService.findById(idProposal));
        featuresRepository.saveAndFlush(feature);
        // Long id = 1; // feature.getId();
        //return id;
    }
    public void deleteById(Long id) {
        featuresRepository.deleteById(id);
    }

    public List<Features> saveFromJson(Features[] features, Long id) {
        List<Features> featuresList = new ArrayList<>();
        for (Features feature : features) {
            feature.setProposal_id(proposalService.findById(id));
            featuresList.add(this.save(feature));
        }
        return featuresList;
    }
}
