package edu.proposalbooster.database.service.Presentation;

import edu.proposalbooster.database.model.Customers.Customers;
import edu.proposalbooster.database.model.Features.Features;
import edu.proposalbooster.database.model.Proposals.Proposals;
import edu.proposalbooster.database.service.Customers.CustomersService;
import edu.proposalbooster.database.service.Features.FeatureService;
import edu.proposalbooster.database.service.Upload.UploadService;
import org.apache.poi.sl.usermodel.TableCell;
import org.apache.poi.sl.usermodel.TextParagraph;
import org.apache.poi.xslf.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.io.*;
import java.util.List;

@Service
public class PresentationService {

    @Autowired
    private FeatureService featureService;
    @Autowired
    private UploadService uploadService;
    @Autowired
    private CustomersService customersService;
    @Autowired
    private ResourceLoader resourceLoader;

    private File file = null;

    private XSLFSlideLayout tablesTemplate;
    private XSLFSlideLayout endTemplate;
    private XSLFSlideLayout proposalTemplate;

    private XMLSlideShow PPTTemplate() {
        XMLSlideShow ppt = null;
        try {
            if (file == null) {
                file = resourceLoader.getResource("classpath:presentation/Proposal.pptx").getFile();
            }
            try(FileInputStream ifstream = new FileInputStream(file)) {
                ppt = new XMLSlideShow(ifstream);
                List<XSLFSlideMaster> slideMasters = ppt.getSlideMasters();
                tablesTemplate = slideMasters.get(0).getSlideLayouts()[11];
                endTemplate = slideMasters.get(0).getSlideLayouts()[17];
                proposalTemplate = slideMasters.get(0).getSlideLayouts()[22];
            } catch (FileNotFoundException e) {
                System.out.println("Working Directory = " + System.getProperty("user.dir"));
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ppt;
    }

    public ResponseEntity<Resource> compilePresentation(Proposals proposal) {
        try {
            XMLSlideShow ppt = PPTTemplate();
            firstSlide(proposal, ppt);
            proposalSlide(proposal, ppt);
            tableSlides(proposal, ppt);
            endSlide(ppt);
            File outPutFile = writeFile(proposal, ppt);
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + outPutFile.getName() + "\"")
                    .body(uploadService.loadFile(outPutFile.toPath()));
        }
        catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    public void deletePresentation(String presentation) {
        try {
            String store = file.getParent();
            File filePresentation = new File(store + "/" + presentation);
            filePresentation.delete();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void firstSlide(Proposals proposal, XMLSlideShow ppt) {
        List<XSLFSlide> allSlides = ppt.getSlides();
        XSLFSlide slide = allSlides.get(0);
        List<XSLFShape> shapes = slide.getShapes();
        XSLFTextShape text = (XSLFTextShape)shapes.get(0);
        text.setText(proposal.getTitle()).setFontSize(55d);
    }

    private void proposalSlide(Proposals proposals, XMLSlideShow ppt) {
        List<Customers> customers = customersService.getAllByProposalId(Long.toString(proposals.getId()));

        XSLFSlide slide = ppt.createSlide(proposalTemplate);
        List<XSLFShape> shapes = slide.getShapes();

        XSLFShape shape;
        XSLFTextRun run;
        XSLFTextParagraph text;

        shape = shapes.get(0);
        XSLFTextShape description = (XSLFTextShape)shape;
        description.clearText();
        description.addNewTextParagraph().setBullet(false);
        run = description.appendText("Details:", false);
        run.setFontSize(20d);
        run.setBold(true);

        shape = shapes.get(1);
        XSLFTextShape details = (XSLFTextShape)shape;
        details.clearText();
        details.addNewTextParagraph().setBullet(false);
        run = details.appendText("Description:", false);
        run.setBold(true);
        run.setFontSize(20d);

        shape = shapes.get(2);
        XSLFTextShape nameShape = (XSLFTextShape)shape;
        nameShape.clearText();
        nameShape.setText(proposals.getTitle() + ": Product Proposal Summary").setFontSize(30d);

        shape = shapes.get(4);
        XSLFTextShape descriptionShape = (XSLFTextShape)shape;
        descriptionShape.clearText();
        descriptionShape.addNewTextParagraph().setBullet(false);
        run = descriptionShape.appendText(proposals.getDescription(), false);
        run.setFontSize(20d);
        run.setBold(false);

        shape = shapes.get(3);
        XSLFTextShape detailsShape = (XSLFTextShape)shape;
        detailsShape.clearText();
        detailsShape.addNewTextParagraph().addNewTextRun().setText("Title: " + proposals.getTitle());
        detailsShape.addNewTextParagraph().addNewTextRun().setText("Timeline: " + proposals.getStart_date() + " - " + proposals.getEnd_date());
        detailsShape.addNewTextParagraph().addNewTextRun().setText("Total LOE: " + proposals.getMan_days());
        detailsShape.addNewTextParagraph().addNewTextRun().setText("Expected profit: " + proposals.getExpected_profit());

        if (customers.size() == 0) {
            detailsShape.addNewTextParagraph().addNewTextRun().setText("There isn't any customers for this proposal!");
        }
        else {
            int max = (customers.size() > 6) ? 6 : customers.size();
            detailsShape.addNewTextParagraph().addNewTextRun().setText("Customers:");
            for (int i = 0; i < max; i++) {
                Customers customer = customers.get(i);
                text = detailsShape.addNewTextParagraph();
                text.setIndentLevel(1);
                text.addNewTextRun().setText(customer.getName());
            }
        }
    }

    private void addHeaderToTable(XSLFTable tbl) {
        int numColumnsTable = 5;
        XSLFTableRow headerRow = tbl.addRow();
        headerRow.setHeight(10);
        for (int i = 0; i < numColumnsTable; i++) {
            XSLFTableCell th = headerRow.addCell();
            XSLFTextParagraph p = th.addNewTextParagraph();
            p.setTextAlign(TextParagraph.TextAlign.CENTER);
            XSLFTextRun r = p.addNewTextRun();
            switch (i) {
                case 0:
                    r.setText("Feature Name");
                    tbl.setColumnWidth(i, 150);
                    break;
                case 1:
                    r.setText("Priority");
                    tbl.setColumnWidth(i, 100);
                    break;
                case 2:
                    r.setText("LOE");
                    tbl.setColumnWidth(i, 75);
                    break;
                case 3:
                    r.setText("Customer");
                    tbl.setColumnWidth(i, 150);
                    break;
                case 4:
                    r.setText("Description");
                    tbl.setColumnWidth(i, 325);
                    break;
            }
            r.setBold(true);
            r.setFontColor(Color.white);
            r.setFontSize(18d);
            th.setFillColor(new Color(62, 101, 143));
            th.setBorderWidth(TableCell.BorderEdge.bottom, 2.0);
            th.setBorderColor(TableCell.BorderEdge.bottom, Color.white);
            th.setBorderWidth(TableCell.BorderEdge.right, 1.0);
            th.setBorderColor(TableCell.BorderEdge.right, Color.white);
        }
    }

    private int countMaxRows(String text, int maxCharacterInRow, int numRowsMax) {
        String[] word = text.split("\\s");
        int size = word.length;
        int tmpMaxCharRow = maxCharacterInRow;
        int rowsInText = 1;
        for (int i = 0; i < size; i++) {
            tmpMaxCharRow -= word[i].length();
            if (tmpMaxCharRow < 0) {
                rowsInText++;
                tmpMaxCharRow = maxCharacterInRow - word[i].length();
            }
            tmpMaxCharRow--;
        }
        numRowsMax -= rowsInText;
        return numRowsMax < 0 ? -1 : numRowsMax;
    }

    private int addTable(XSLFSlide slide, String title, List<Features> list, int start) {
        List<XSLFShape> shapes = slide.getShapes();
        XSLFShape shape = shapes.get(0);
        XSLFTextShape nameShape = (XSLFTextShape)shape;
        nameShape.clearText();
        nameShape.setText(title + ": Feature Scope").setFontSize(30d);

        int currentRow = start;
        int numRowsMax = 16;
        int numColumnsTable = 5;
        int numRowsInList = list.size();
        XSLFTable tbl = slide.createTable();
        tbl.setAnchor(new Rectangle(50, 100, 600, 300));
        addHeaderToTable(tbl);

        for (int rowNum = 0; rowNum < numRowsMax && currentRow < numRowsInList ; rowNum++) {
            Features current = list.get(currentRow);
            String nameProposal = current.getName();
            String description = current.getDescription();
            String nameCustomer = current.getCustomer_id().getName();
            int tmpRowsMaxCustomer, tmpRowsMaxDesc, tmpRowsMaxProposal;
            if ((tmpRowsMaxCustomer = countMaxRows(nameCustomer, 15, numRowsMax)) == -1 ||
                    (tmpRowsMaxDesc = countMaxRows(description, 25, numRowsMax)) == -1 ||
                    (tmpRowsMaxProposal = countMaxRows(nameProposal, 15, numRowsMax)) == -1) {
                break;
            }
            numRowsMax = (tmpRowsMaxCustomer < tmpRowsMaxDesc) ? tmpRowsMaxCustomer : tmpRowsMaxDesc;
            numRowsMax = (numRowsMax < tmpRowsMaxProposal) ? numRowsMax : tmpRowsMaxProposal;

            XSLFTableRow tr = tbl.addRow();
            tr.setHeight(40);

            for (int i = 0; i < numColumnsTable; i++) {
                XSLFTableCell cell = tr.addCell();
                XSLFTextParagraph p = cell.addNewTextParagraph();
                XSLFTextRun r = p.addNewTextRun();

                switch (i) {
                    case 0:
                        r.setText(nameProposal);
                        break;
                    case 1:
                        r.setText(Integer.toString(current.getPriority()));
                        break;
                    case 2:
                        r.setText(String.valueOf(current.getLoe_per_feature()));
                        break;
                    case 3:
                        r.setText(nameCustomer);
                        break;
                    case 4:
                        r.setText(description);
                        break;
                }

                if (rowNum % 2 == 0) {
                    cell.setFillColor(new Color(206, 211, 219));
                } else {
                    cell.setFillColor(new Color(232, 234, 238));
                }
            }
            currentRow++;
            numRowsMax--;
        }
        return currentRow - start;
    }

    private void tableSlides(Proposals proposals, XMLSlideShow ppt) {
        int start = 0;
        List<Features> list = featureService.getAllByProposalId(proposals.getId());
        if (list == null) {
            return;
        }
        int rowsNotInTable = list.size();
        while (rowsNotInTable > 0) {
            int inTable = addTable(ppt.createSlide(tablesTemplate), proposals.getTitle(), list, start);
            start += inTable;
            rowsNotInTable -= inTable;
        }
    }

    private void endSlide(XMLSlideShow ppt) {
        ppt.createSlide(endTemplate);
    }

    private File writeFile(Proposals proposals, XMLSlideShow ppt) throws Exception { ;
        String name = proposals.getTitle();
        String store = file.getParent();
        File outPutFile = new File(store + "/" + name + ".pptx");
        if (outPutFile.exists()) {
            for (int i = 0; i < 100; i++) {
                outPutFile = new File(store + "/" + name + "(" + Integer.toString(i) + ").pptx");
                if (!outPutFile.exists()) {
                    break;
                }
                i++;
            }
            if (outPutFile.exists()) {
                throw new Exception("Error - create presentation");
            }
        }
        try (FileOutputStream out = new FileOutputStream(outPutFile)) {
            ppt.write(out);
            return outPutFile;
        }
    }
}
