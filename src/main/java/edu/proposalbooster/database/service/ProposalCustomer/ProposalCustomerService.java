package edu.proposalbooster.database.service.ProposalCustomer;

import com.google.gson.Gson;

import edu.proposalbooster.database.model.ProposalCustomer.ProposalCustomer;
import edu.proposalbooster.database.model.Proposals.Proposals;
import edu.proposalbooster.database.repository.Customers.CustomersRepository;
import edu.proposalbooster.database.repository.ProposalCustomer.ProposalCustomerRepository;
import edu.proposalbooster.database.repository.Proposals.ProposalsRepository;
import org.json.JSONArray;
import org.springframework.security.core.context.SecurityContextHolder;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ProposalCustomerService {

    @Autowired
    private ProposalCustomerRepository proposalCustomerRepository;

    @Autowired
    private CustomersRepository customersRepository;

    @Autowired
    private ProposalsRepository proposalsRepository;


    public ProposalCustomer createProposalCustomer(JSONObject jsproposal_customer) {
        Gson g = new Gson();
        ProposalCustomer proposalCustomer = g.fromJson(jsproposal_customer.toString(), ProposalCustomer.class);
        int customerPriority = 1;
        proposalCustomer.setCustomerPriority(customerPriority);
        proposalCustomerRepository.save(proposalCustomer);
        return proposalCustomer;
    }

    public void createProposalCustomerArray(JSONArray customers, Proposals proposal) {
        for (int i = 0; i < customers.length(); i++) {
            Long customer_id = (long) customers.getJSONObject(i).getInt("customer_id");
            ProposalCustomer proposalCustomer = new ProposalCustomer();
            int priority = 1;
            proposalCustomer.setCustomer_id(customersRepository.findById(customer_id).get());
            proposalCustomer.setProposalId(proposal);
            proposalCustomer.setCustomerPriority(priority);
            proposalCustomerRepository.save(proposalCustomer);
        }
    }

    @Transactional
    public void deleteAllWithProposalId(Long idLong) {
        proposalCustomerRepository.deleteAllByProposalId(proposalsRepository.findById(idLong).get());
    }

    public List<ProposalCustomer> getAllByProposalId(Long IdLOng) {
        return proposalCustomerRepository.findAllByProposalId(proposalsRepository.findById(IdLOng).get());
    }

}