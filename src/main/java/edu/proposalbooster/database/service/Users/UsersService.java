package edu.proposalbooster.database.service.Users;

import edu.proposalbooster.database.model.Users.Users;

import java.util.List;
import java.util.Optional;

public interface UsersService {

    Users insert(Users usr);

    Users update(Users usr, long id);

    void deleteUserById(Long id);

    List<Users> loadAllUsers();

    Users getUserById(long user_id);

    Users getUserByLogin(String login);

    Users getUserByRole(String role);

    Long getUserIdByLogin(String login);

    String getUserRoleById(Long id);
}

