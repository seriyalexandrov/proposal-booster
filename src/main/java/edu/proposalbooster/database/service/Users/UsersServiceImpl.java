package edu.proposalbooster.database.service.Users;

import edu.proposalbooster.database.model.Users.Users;
import edu.proposalbooster.database.repository.Users.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersServiceImpl implements UsersService {

    @Autowired
    UsersRepository usersRepository;

    @Override
    public Users insert(Users usr) {
        return usersRepository.save(usr);
    }

    @Override
    public Users update(Users usr, long id) {
        return usersRepository.save(usr);
    }

    @Override
    public List<Users> loadAllUsers() {
        return usersRepository.findAll();
    }

    @Override
    public Users getUserById(long user_id) {
        return usersRepository.findById(user_id).orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public Users getUserByLogin(String login) {
        return usersRepository.findByLogin(login).orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public Users getUserByRole(String role) {
        return usersRepository.getUsersByRole(role);
    }

    @Override
    public void deleteUserById(Long id) {
        usersRepository.deleteById(id);
    }

    @Override
    public Long getUserIdByLogin(String login){
        Users user = usersRepository.findByLogin(login).orElseThrow(IllegalArgumentException::new);
        return user.getId();
    }

    @Override
    public String getUserRoleById(Long id){
        Users user = usersRepository.getUserById(id);
        String roleUser = user.getRole();
        return roleUser;
    }
}
