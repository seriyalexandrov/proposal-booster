package edu.proposalbooster.database.service.Customers;

import edu.proposalbooster.database.model.Customers.Customers;
import edu.proposalbooster.database.model.Proposals.AddtionalFields.AdditionalFields;
import edu.proposalbooster.database.repository.Customers.CustomersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CustomersService {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private CustomersRepository customerRepository;

    public List<String> getAllCustomers(){
        return customerRepository.findAll().stream()
                .map(Customers::getName).
                        collect(Collectors.toList());
    }

    /*public List<Customers> getAllByProposalId(String proposalId) {
        Query query = entityManager.createNativeQuery(
                "select c.* from Proposals_customer pc INNER JOIN Customers c on c.customer_id = pc.customer_id where pc.proposal_id = ?1");
        query.setParameter(1, new BigInteger(proposalId));
        return query.getResultList();
    }*/

    public List<Customers> getAllByProposalId(String proposalId) {
        Query query = entityManager.createNativeQuery(
                "select c.* from Proposals_customer pc INNER JOIN Customers c on c.customer_id = pc.customer_id where pc.proposal_id = ?1");
            query.setParameter(1, new BigInteger(proposalId));
        List<Object[]> result = query.getResultList();
        List<Customers> convertedResults = new ArrayList<>(result.size());
        for(Object[] row : result) {
            convertedResults.add(new Customers((BigInteger)row[0], (String)row[1]));
        }
        return convertedResults;
    }

    public List<Customers> getAll(){
        return customerRepository.findAll();
    }

    public Customers createCustomer(Customers customer) {
        Optional<Customers> optionalCustomers = customerRepository.findByName(customer.getName());
        if (optionalCustomers.isPresent()) {
            return optionalCustomers.get();
        }
        return customerRepository.save(customer);
    }

    public Customers findById(Long id) {
        return customerRepository.findById(id).get();
    }

    public void deleteById(Long id) {
        customerRepository.deleteById(id);
    }

    public void update(Customers customer) {
        customerRepository.save(customer);
    }
}