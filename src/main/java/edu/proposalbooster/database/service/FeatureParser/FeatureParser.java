package edu.proposalbooster.database.service.FeatureParser;

import edu.proposalbooster.database.model.Customers.Customers;
import edu.proposalbooster.database.model.Features.Features;
import edu.proposalbooster.database.repository.Customers.CustomersRepository;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Service
public class FeatureParser {
    @Autowired
    CustomersRepository customersRepository;

    public List<Features> parse(InputStream inputStream) {
        try {
            Workbook workbook = WorkbookFactory.create(inputStream);
            return parse(workbook);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Features> parse(Workbook workbook) {
        try {
            Sheet sheet = workbook.getSheetAt(0);
            ArrayList<Features> features = new ArrayList<>();

            System.out.println("\nParsing features...");
            sheet.forEach(row -> {
                Cell cellName = row.getCell(0);
                Cell cellPriority = row.getCell(1);
                Cell cellCustomer = row.getCell(2);
                Cell cellLoe = row.getCell(3);
                Cell cellDescription = row.getCell(4);
                if (isValidFormat(row)) {
                    String name = cellName.getStringCellValue();
                    int priority = (int) cellPriority.getNumericCellValue();
                    double loe = cellLoe.getNumericCellValue();
                    String decription = cellDescription.getStringCellValue();
                    Customers customer = null;
                    if (cellCustomer.getCellType() == CellType.STRING) {
                        if ( customersRepository.findByName(cellCustomer.getStringCellValue()).isPresent()) {
                            customer = customersRepository.findByName(cellCustomer.getStringCellValue()).get();
                        }
                    } else if(cellCustomer.getCellType() == CellType.NUMERIC) {
                        if ( customersRepository.findById((long) cellCustomer.getNumericCellValue()).isPresent()) {
                            customer = customersRepository.findById((long) cellCustomer.getNumericCellValue()).get();
                        }
                    }
                    features.add(new Features(name, priority, loe, decription, customer));
                } else {
                    System.out.println("Oh format is invalid:(((");
                }
            });
            workbook.close();
            return features;
        } catch (IOException e) {
            e.printStackTrace(); // ERROR BAD FORMAT
        }
        return null;
    }

    public List<Features> parse(String fileName) {
        try {
            Workbook workbook = WorkbookFactory.create(new File(fileName));
            return parse(workbook);
        }
        catch(IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean isValidFormat(Row row) {
        Cell cellName = row.getCell(0);
        Cell cellPriority = row.getCell(1);
        Cell cellCustomer = row.getCell(2);
        Cell cellLoe = row.getCell(3);
        Cell cellDescription = row.getCell(4);
        return cellName.getCellType() == CellType.STRING && cellPriority.getCellType() == CellType.NUMERIC &&
                (cellCustomer.getCellType() == CellType.STRING || cellCustomer.getCellType() == CellType.NUMERIC) &&
                cellLoe.getCellType() == CellType.NUMERIC && cellDescription.getCellType() == CellType.STRING;
    }
}
