package edu.proposalbooster.database.model.ProposalCustomer;

import edu.proposalbooster.database.model.Customers.Customers;
import edu.proposalbooster.database.model.Proposals.Proposals;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "Proposals_customer", uniqueConstraints = {@UniqueConstraint(columnNames={"customer_id", "proposalId"})})
public class ProposalCustomer implements Serializable {
    //    todo  ADD ID for each record
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "proposal_customer_id")
    // @OneToMany(mappedBy = "proposalId")
    @Getter
    @Setter
    private Long id;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    //  @Column(name = "customer_id")
    @Getter
    @Setter
    private Customers customer_id;

// todo check affects of parameters insertable = false, updatable = false

    @ManyToOne
    @JoinColumn(name = "proposalId")
    // @Column(name = "proposalId")
    @Getter
    @Setter
    private Proposals proposalId;

    @Column(name = "customerPriority")
    //  @Column(name = "customerPriority")
    @Getter
    @Setter
    private int customerPriority;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof ProposalCustomer)) return false;

        return Objects.equals(getCustomer_id(), ((ProposalCustomer) o).getCustomer_id()) &&
                Objects.equals(getProposalId(), ((ProposalCustomer) o).getProposalId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getProposalId(), getCustomer_id());
    }
}
