package edu.proposalbooster.database.model.Customers;

import edu.proposalbooster.database.model.Features.Features;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.List;

@Entity
@Table(name = "Customers")
public class Customers {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "customer_id")
    @Getter
    private Long customer_id;

    @Column(name = "name")
    @Getter
    @Setter
    private String name;

//    @OneToMany (mappedBy = "customer_id")
//    @Getter
//    @Setter
//    private List<Features> features;

    public Customers(BigInteger id, String name) {
        this.customer_id = id.longValue();
        this.name = name;
    }

    public Customers() {
    }
}
