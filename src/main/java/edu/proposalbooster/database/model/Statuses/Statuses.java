package edu.proposalbooster.database.model.Statuses;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "Statuses")
public class Statuses {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "status_id")
    @Getter
    private Long status_id;

    @Column(name = "name")
    @Getter
    @Setter
    private String name = "name";

    @Column(name = "description")
    @Getter
    @Setter
    private String description = "description";

    public Statuses(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Statuses(long id){
        this.status_id = id;

        switch((int)id){
            case 1: this.name = "In Work";
                    this.description = "In Work";
                    break;
            case 2: this.name = "In Queue";
                    this.description = "In Queue";
                    break;
            case 3: this.name = "Approved";
                this.description = "Approved";
                break;
            case 4: this.name = "Archived";
                    this.description = "Archived";
                    break;
        }
    }

    public Statuses() {
    }

    @Override
    public String toString() {
        return name;
    }
}
