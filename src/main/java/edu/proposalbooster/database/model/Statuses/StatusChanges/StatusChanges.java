package edu.proposalbooster.database.model.Statuses.StatusChanges;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "StatusChanges")
@Getter
@Setter
public class StatusChanges {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "change_id")
    private Long change_id;


    @Column(name = "status1Id")
    private Long status1Id;


    @Column(name = "status2Id")
    private Long status2Id;

    public StatusChanges() {
    }
}
