package edu.proposalbooster.database.model.Users;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "users")
public class Users implements Serializable {

    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Getter
    @Setter
    private Long id;

    @Column(name = "firstname")
    @Getter
    @Setter
    private String firstName = "None";


    @Column(name = "lastname")
    @Getter
    @Setter
    private String lastName = "None";

    @Column(name = "login", unique = true)
    @NotNull
    @Getter
    @Setter
    private String login = "None";

    @Column(name = "password", nullable = false)
    @Getter
    @Setter
    private String password = "password";

    @Column(name = "role", nullable = false)
    @Getter
    @Setter
    private String role;

    @Column(name = "email")
    @Getter
    @Setter
    private String email = "None";

    public Users(String firstname, String surname, String login, String password, String role) {
        this.firstName = firstname;
        this.lastName = surname;
        this.login = login;
        this.password = password;
        this.role = role;
    }

    public Users(String firstName, String secondName) {
        this.firstName = firstName;
        this.lastName = secondName;
    }

    public Users() {

    }

    public Users(long id){
        this.id = id;
    }

    @Override
    public String toString() {
        return firstName + " " +  lastName + " " ;
    }

    public String getLogin() {
        return login;
    }

    public String getEmail() {
        return email;
    }

    public Long getId(){
        return id;
    }

    public String getRole(){ return  role;}
}
