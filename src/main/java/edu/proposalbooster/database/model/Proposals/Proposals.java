package edu.proposalbooster.database.model.Proposals;

import edu.proposalbooster.database.model.Features.Features;
import edu.proposalbooster.database.model.Proposals.AddtionalFields.AdditionalFields;
import edu.proposalbooster.database.model.Statuses.Statuses;
import edu.proposalbooster.database.model.Users.Users;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static java.lang.Long.parseLong;

@TypeDef(name = "JsonAdditionalFields", typeClass = AdditionalFields[].class)

@Entity(name = "Proposals")
@Table(name = "Proposals")
public class Proposals {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "proposal_id")
   // @OneToMany(mappedBy = "proposal_id")
    @Getter
    @Setter
    private Long id;

    @Column(name = "title")
    @Getter
    @Setter
    private String title;

    @Column(name = "description")
    @Getter
    @Setter
    private String description;

    @Column(name = "expected_profit")
    @Getter
    @Setter
    private String expected_profit;

    @Column(name = "man_days")
    @Getter
    @Setter
    private Long man_days;

    @Column(name = "start_date")
    @Getter
    @Setter
    private String start_date;

    @Column(name = "end_date")
    @Getter
    @Setter
    private String end_date;

    @Column(name = "equipment")
    @Getter
    @Setter
    private String equipment;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @Getter
    @Setter
    private Users user_id;

    @Getter
    @Setter
    @Type(type = "JsonAdditionalFields")
    private AdditionalFields[] additionalFields;

    @ManyToOne
    @JoinColumn(name = "status")
    @Getter
    @Setter
    private Statuses status;

    @Column(name = "reject_message")
    @Getter
    @Setter
    private String reject_message;

//    @OneToMany (mappedBy = "proposal_id")
//    @Getter
//    @Setter
//    private List<Features> features;

    public Proposals() {
    }


    public Proposals(BigInteger id, AdditionalFields[] additionalFields, String description, String end_date, String equipment, String expected_profit, BigInteger man_days, String reject_message, String start_date, String title, BigInteger status, BigInteger user_id ) {
        long longID = id.longValue();
        this.id = new Long(longID);
        this.title = title;
        this.description = description;
        this.expected_profit = expected_profit;
        if (man_days == null){
            this.man_days = null;
        }
        else{
            long longManDays = man_days.longValue();
            this.man_days = new Long(longManDays);
        }
        this.start_date = start_date;
        this.end_date = end_date;
        this.equipment = equipment;
        long user = user_id.longValue();
        this.user_id = new Users(user);
        this.additionalFields = additionalFields;
        long stat = status.longValue();
        this.status = new Statuses(stat);
        this.reject_message = reject_message;
    }

    public Proposals(Long id, String title, String description, String expected_profit, Long man_days, String start_date, String end_date, String equipment, Users user_id, AdditionalFields[] additionalFields,  Statuses status, String reject_message) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.expected_profit = expected_profit;
        this.man_days = man_days;
        this.start_date = start_date;
        this.end_date = end_date;
        this.equipment = equipment;
        this.user_id = user_id;
        this.additionalFields = additionalFields;
        this.status = status;
        this.reject_message = reject_message;
    }

    public Proposals(Proposals proposals) {
        this.title = proposals.title;
        this.description = proposals.description;
        this.expected_profit = proposals.expected_profit;
        this.man_days = proposals.man_days;
        this.start_date = proposals.start_date;
        this.end_date = proposals.end_date;
        this.equipment = proposals.equipment;
        this.user_id = proposals.user_id;
        this.additionalFields = proposals.additionalFields;
        this.status = proposals.status;
    }

    public Proposals(AdditionalFields[] additionalFields) {
        this.additionalFields = additionalFields;
    }

    public String getTitle() {
        return title;
    }
    public String getDescription(){
        return description;
    }
    public String getExpected_profit(){
        return expected_profit;
    }
    public Long getMan_days(){
        return man_days;
    }
    public String getStart_date(){
        return start_date;
    }
    public String getEnd_date(){
        return end_date;
    }
    public String getEquipment (){
        return equipment;
    }
    public Users getUser_id() { return user_id;}
    public AdditionalFields[] getAdditionalFields() { return additionalFields; }
    public Statuses getStatus() { return status; }
    public String getReject_message() { return reject_message; }

    public void setTitle(String title) { this.title = title; }
    public void setDescription(String description) { this.description = description;}
    public void setExpected_profit(String expected_profit) { this.expected_profit = expected_profit;}
    public void setMan_days(Long man_days) { this.man_days = man_days; }
    public void setStart_date(String start_date) { this.start_date = start_date; }
    public void setEnd_date(String end_date) { this.end_date = end_date; }
    public void setEquipment(String equipment) { this.equipment = equipment; }
    public void setUser_id(Users user_id) { this.user_id = user_id; }
    public void setAdditionalFields(AdditionalFields[] additionalFields) { this.additionalFields = additionalFields; }
    public void setStatus(Statuses status) { this.status = status; }
    public void setReject_message(String reject_message) { this.reject_message = reject_message; }

//    public void addFeature(Features feature) {
//        features.add(feature);
//    }
//    public void addFeature(List<Features> features) {
//        features = new ArrayList<>();
//        features.addAll(features);
//    }
}
