package edu.proposalbooster.database.model.Proposals.AddtionalFields;

import org.hibernate.dialect.PostgreSQL9Dialect;

import java.lang.reflect.Type;
import java.sql.Types;

public class PostgresDialect extends PostgreSQL9Dialect {

    public PostgresDialect() {
        this.registerColumnType(Types.JAVA_OBJECT, "jsonb");
    }
}
