package edu.proposalbooster.database.model.Proposals.AddtionalFields;

import java.io.Serializable;

public class AdditionalFields implements Serializable {

    private String field;
    private String description;

    public AdditionalFields(String field, String description) {
        this.field = field;
        this.description = description;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
