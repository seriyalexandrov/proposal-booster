package edu.proposalbooster.database.model.Features;

import edu.proposalbooster.database.model.Customers.Customers;
import edu.proposalbooster.database.model.Proposals.Proposals;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table(name = "Features")
public class Features {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "feature_id")
    @Getter
    private Long feature_id;

    @Column(name = "name")
    @Getter
    @Setter
    private String name;

    @Column(name = "priority")
    @Getter
    @Setter
    private Integer priority;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    @Getter
    @Setter
    private Customers customer_id;

    public void setProposal_id(Proposals proposal_id) {
        this.proposal_id = proposal_id;
    }

    @ManyToOne
    @JoinColumn(name = "proposal_id")
    @Getter
    @Setter
    private Proposals proposal_id;

    @Column(name = "loe_per_feature")
    @Getter
    @Setter
    private double loe_per_feature;

    @Column(name = "description")
    @Getter
    @Setter
    private String description;

    public Features(String name) {
        this.name = name;
    }

    public Features() {
    }

    public Features(String name, int priority, double loe, String description, Customers customers) {
        this.name = name;
        this.priority = priority;
        this.customer_id = customers;
        this.loe_per_feature = loe;
        this.description = description;
    }

    public Features(BigInteger feature_id, String name, Integer priority, Proposals proposal, double loe, String description, Customers customer) {
        this.feature_id = feature_id.longValue();
        this.name = name;
        this.priority = priority;
        this.customer_id = customer;
        this.proposal_id = proposal;
        this.loe_per_feature = loe;
        this.description = description;
    }

    public Features(String name, int priority, Proposals proposal, double loe, String description, Customers customers) {
        this.name = name;
        this.priority = priority;
        this.customer_id = customers;
        this.proposal_id = proposal;
        this.loe_per_feature = loe;
        this.description = description;
    }

//    @Override
//    public String toString() {
//        return "FEATURE ID:" + Long.toString(feature_id)
//                + "\nname:" + name
//                + "\npriority:" + Integer.toString(priority)
//                + "\nCustomer:" + customer_id.toString()
//                + "\nProposal:" + proposal_id.toString()
//                + "\nloe:" + Double.toString(loe_per_feature)
//                + "\ndescription:" + description;
//    }

    public String toString() {
        return  "name:" + name
                + "\npriority:" + Integer.toString(priority)
                + "\nProposal:" + proposal_id.toString()
                + "\nloe:" + Double.toString(loe_per_feature)
                + "\ndescription:" + description;
    }
}
