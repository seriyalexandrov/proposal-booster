package edu.proposalbooster.database.repository.ProposalCustomer;

import edu.proposalbooster.database.model.ProposalCustomer.ProposalCustomer;
import edu.proposalbooster.database.model.Proposals.Proposals;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ProposalCustomerRepository extends CrudRepository<ProposalCustomer, Long> {

    Optional<ProposalCustomer> findById(Long id);

    ProposalCustomer save(ProposalCustomer entity);

    void deleteById(Long id);

    void deleteAllByProposalId(Proposals ProposalsId);

    List<ProposalCustomer> findAllByProposalId(Proposals ProposalsId);

}