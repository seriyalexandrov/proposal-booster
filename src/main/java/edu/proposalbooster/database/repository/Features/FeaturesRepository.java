package edu.proposalbooster.database.repository.Features;

import edu.proposalbooster.database.model.Features.Features;
import edu.proposalbooster.database.model.Proposals.Proposals;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface FeaturesRepository extends CrudRepository<Features, Long> {

    List<Features> findAll();

    Optional<Features> findById(Long id);



    Optional<Features> findByName(String name);

    Features save(Features entity);

    Features saveAndFlush(Features proposals);

    void deleteById(Long id);
}