package edu.proposalbooster.database.repository.Users;
import edu.proposalbooster.database.model.Users.Users;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UsersRepository extends CrudRepository<Users, Long> {

    List<Users> findAll();

    Optional<Users> findById(Long id);

    Users save(Users entity);

    void deleteById(Long id);

    Optional<Users> findByLogin(String login);

    Users getUsersByRole(String role);

    Users getUserById(Long id);

}
