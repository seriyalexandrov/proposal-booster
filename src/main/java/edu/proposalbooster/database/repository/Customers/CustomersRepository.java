package edu.proposalbooster.database.repository.Customers;

import edu.proposalbooster.database.model.Customers.Customers;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface CustomersRepository extends CrudRepository<Customers, Long> {

    List<Customers> findAll();

    Optional<Customers> findById(Long id);

    Optional<Customers> findByName(String name);

    Customers save(Customers entity);

    void deleteById(Long id);

}
