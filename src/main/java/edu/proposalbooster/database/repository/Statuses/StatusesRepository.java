package edu.proposalbooster.database.repository.Statuses;
import edu.proposalbooster.database.model.Statuses.Statuses;
import edu.proposalbooster.database.model.Users.Users;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface StatusesRepository extends CrudRepository<Statuses, Long> {

    List<Statuses> findAll();

    Optional<Statuses> findById(Long id);

    Optional<Statuses> findByName(String name);

  //  List<User> findByLastName(String lastName);

    Statuses save(Statuses entity);

    void deleteById(Long id);

    Statuses getStatusesByName(String name);
}
