package edu.proposalbooster.database.repository.Statuses;

import edu.proposalbooster.database.model.Statuses.StatusChanges.StatusChanges;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface StatusChangesRepository extends CrudRepository<StatusChanges, Long> {

        List<StatusChanges> findAll();

        Optional<StatusChanges> findByStatus1IdAndStatus2Id(long status1_id, long status2_id);

}
