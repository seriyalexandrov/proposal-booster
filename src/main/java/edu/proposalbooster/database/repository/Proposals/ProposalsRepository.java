package edu.proposalbooster.database.repository.Proposals;

import edu.proposalbooster.database.model.Proposals.Proposals;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ProposalsRepository extends CrudRepository<Proposals, Long> {

    List<Proposals> findAll();

    Optional<Proposals> findById(Long id);

    Proposals save(Proposals entity);

    Proposals saveAndFlush(Proposals proposals);

    void deleteById(Long id);

    //Optional<Proposals> findByUser_id(Long user_id);
}
