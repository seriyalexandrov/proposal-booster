package edu.proposalbooster.mvc.Feature;

import edu.proposalbooster.database.model.Features.Features;
import edu.proposalbooster.database.service.Features.FeatureService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class FeatureController {

    @Autowired
    private FeatureService featureService;

    @PostMapping("/feature")
    public @ResponseBody
    void createFeature(@RequestBody(required = false) String feature) throws Exception {
        JSONObject json = new JSONObject(feature);
        JSONObject feature1 = json.getJSONObject("feature");
        Long id = null;
        featureService.createFeature(feature1, id);
    }

    @GetMapping("/features/{id}")
    public List<Features> getFeaturesByProposalId(@PathVariable Long id) {
        return featureService.getAllByProposalId(id);
    }
}