package edu.proposalbooster.mvc.Security;

import edu.proposalbooster.database.service.Users.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequestMapping("security")
public class SecurityController {
    @Autowired
    UsersService users;

    @RequestMapping(value = "/logout")
    public void logoutPage(HttpServletRequest request) {
        try {
            request.logout();
        } catch (ServletException e) {
            e.printStackTrace();
        }
    }

    @GetMapping(value = "/roles")
    public Collection<String> getRoles() {
        return SecurityContextHolder.getContext()
                .getAuthentication()
                .getAuthorities()
                .stream()
                .map(x -> x.toString())
                .collect(Collectors.toList());
    }
}
