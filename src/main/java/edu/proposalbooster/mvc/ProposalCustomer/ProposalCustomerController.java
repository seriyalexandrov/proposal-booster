package edu.proposalbooster.mvc.ProposalCustomer;

import edu.proposalbooster.database.model.ProposalCustomer.ProposalCustomer;
import edu.proposalbooster.database.service.ProposalCustomer.ProposalCustomerService;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProposalCustomerController {

    @Autowired
    private ProposalCustomerService proposalCustomerService;

    @PostMapping("/proposalCustomer")
    public @ResponseBody ProposalCustomer createProposalCustomer(@RequestBody(required = false) String proposalCustomer) throws JSONException {
        JSONObject json = new JSONObject(proposalCustomer);
        JSONObject jsproposalCustomer = json.getJSONObject("proposalCustomer");
        return proposalCustomerService.createProposalCustomer(jsproposalCustomer);
    }

    @GetMapping("/customersById/{id}")
    public @ResponseBody
    List<ProposalCustomer> getCastomersByProposalId(@PathVariable Long id) throws JSONException {
        return proposalCustomerService.getAllByProposalId(id);
    }
}
