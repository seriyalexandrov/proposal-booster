package edu.proposalbooster.mvc.Users;

import edu.proposalbooster.database.model.Users.Users;
import edu.proposalbooster.database.service.Users.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("users")
public class UsersController {

    @Autowired
    private UsersService usersService;

    //TODO : Должна работать без авторизации !
    @PostMapping("/create")
    public Users createUser(@RequestBody Users requestUserDetails) {
        usersService.insert(requestUserDetails);
        return requestUserDetails;
    }

    @GetMapping("/id_{id}")
    public Users getById(@PathVariable String id) {
        return usersService.getUserById(Long.parseLong(id));
    }

    @GetMapping("/login_{login}")
    public Users getByLogin(@PathVariable String login) {
        return usersService.getUserByLogin(login);
    }

    @GetMapping("/all")
    public List<Users> getAll() {
        return usersService.loadAllUsers();
    }

    @DeleteMapping("/delete_{id}")
    public ResponseEntity deleteUser(@PathVariable String id) {
        usersService.deleteUserById(Long.parseLong(id));
        return ResponseEntity.ok().build();
    }

    @PutMapping("/update_{id}")
    public Users updateUser(@PathVariable String id, @RequestBody Users requestUserDetails) {
        return usersService.update(requestUserDetails, Long.parseLong(id));
    }

    @GetMapping("/userId")
    public Long getUserId(@PathVariable String login){return usersService.getUserIdByLogin(login);}

}
