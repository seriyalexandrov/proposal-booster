package edu.proposalbooster.mvc.Upload;

import java.util.ArrayList;
import java.util.InvalidPropertiesFormatException;
import java.util.List;

import edu.proposalbooster.database.model.Features.Features;
import edu.proposalbooster.database.service.FeatureParser.FeatureParser;
import edu.proposalbooster.database.service.Presentation.PresentationService;
import edu.proposalbooster.database.service.Proposal.ProposalService;
import edu.proposalbooster.database.service.Upload.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("upload")
public class UploadController {

    @Autowired
    private ProposalService proposalService;
    @Autowired
    private PresentationService presentationService;
    @Autowired
    private UploadService uploadService;
    @Autowired
    private FeatureParser featureParser;

    private final String extension = ".xlsx";

    @RequestMapping(value = "/post", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<List<Features>> handleFileUpload(
            @RequestParam(value="file") MultipartFile file,
            @RequestParam(value="proposalName") String proposalName) {
        String message;
        List<Features> listFeatures = new ArrayList<>();
        try {
            int length = file.getOriginalFilename().length();
            if (!file.getOriginalFilename().substring(length - extension.length(), length).equals(extension)) {
                throw new InvalidPropertiesFormatException("Error - bad file extension");
            }
            listFeatures = featureParser.parse(file.getInputStream());
            return ResponseEntity.status(HttpStatus.OK).body(listFeatures);
        } catch (InvalidPropertiesFormatException e) {
            message = "Bad extension " + file.getOriginalFilename();
            listFeatures.add(new Features(null, -1, null, 0, message, null));
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(listFeatures);
        }
        catch(Exception e) {
            message = "Fail to upload " + file.getOriginalFilename();
            listFeatures.add(new Features(null, -1, null, 0, message, null));
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(listFeatures);
        }
    }

    @RequestMapping(value = "/store", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<String> handleFileUpload(@RequestParam(value="file") MultipartFile file) {
        try {
            String uploaded = uploadService.storeFile(file);
            return ResponseEntity.status(HttpStatus.OK).body("Uploaded file " + uploaded);
        }
        catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body("Error");
        }
    }

    @GetMapping("/file/{fileName}")
    @ResponseBody
    public ResponseEntity<Resource> getFile(@PathVariable String fileName) {
        Resource file = uploadService.loadFile(fileName);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                .body(file);
    }

    @GetMapping("/pptx")
    @ResponseBody
    public ResponseEntity<Resource> createPPTX(@RequestParam("id") Long id) {
        return presentationService.compilePresentation(proposalService.findById(id));
    }

    @PostMapping("/deletepptx")
    @ResponseBody
    public void deletePPTX(@RequestParam("file") String presentation) {
        presentationService.deletePresentation(presentation);
    }
}