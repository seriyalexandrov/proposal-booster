package edu.proposalbooster.mvc.Customer;

import edu.proposalbooster.database.model.Customers.Customers;
import edu.proposalbooster.database.service.Customers.CustomersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CustomerController {

    @Autowired
    private CustomersService customersService;

    @GetMapping("/customers")
    public List<Customers> getAllCustomers() {
        return customersService.getAll();
    }

    @GetMapping("/customers/{id}")
    public List<Customers> getCustomersByProposalId(@PathVariable String id) {
        return customersService.getAllByProposalId(id);
    }

    @PostMapping("/customers")
    public @ResponseBody Customers createCustomer(@RequestBody Customers customer) {
        return customersService.createCustomer(customer);
    }
}
