package edu.proposalbooster.mvc.Proposal;

import com.fasterxml.jackson.core.JsonParser;
import com.google.gson.Gson;
import com.sun.org.apache.xpath.internal.operations.Bool;
import edu.proposalbooster.database.model.Customers.Customers;
import edu.proposalbooster.database.model.Features.Features;
import edu.proposalbooster.database.model.Proposals.Proposals;

import edu.proposalbooster.database.model.Statuses.Statuses;
import edu.proposalbooster.database.service.Customers.CustomersService;
import edu.proposalbooster.database.service.Features.FeatureService;
import edu.proposalbooster.database.service.Proposal.ProposalService;
import edu.proposalbooster.database.service.ProposalCustomer.ProposalCustomerService;
import edu.proposalbooster.database.service.Users.UsersService;
import edu.proposalbooster.mvc.Feature.FeatureController;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static java.lang.Integer.parseInt;

@RestController
public class ProposalController {

    @Autowired
    private ProposalService proposalService;
    @Autowired
    private ProposalCustomerService proposalCustomerService;
    @Autowired
    private FeatureService featureService;
    @Autowired
    private UsersService usersService;

    @PutMapping("/proposalUpdate/{id}")
    public @ResponseBody ResponseEntity changeStatus(@PathVariable String id, @RequestBody(required = false) String jsonBody) {
        JSONObject json = new JSONObject(jsonBody);
        JSONObject proposal1 = json.getJSONObject("proposal");
        String status = json.getString("newStatus");
        Long proposalId = Long.parseLong(id);

        boolean changeMessage = false;
        if (json.has("isMessage")) {
            changeMessage = json.getBoolean("isMessage");
        }

        try {
            System.out.println("Error mirror!!!");
            proposalService.changeStatusOfProposal(proposalId, proposal1, status, changeMessage);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/proposals/{id}")
    public @ResponseBody ResponseEntity updateProposal(@PathVariable String id, @RequestBody String jsonBody) {
        Long proposalId = Long.parseLong(id);
        JSONObject json = new JSONObject(jsonBody);
        JSONObject proposal1 = json.getJSONObject("proposal");
        String status = json.getString("newStatus");
        JSONArray customersArr = json.getJSONArray("customers");
        try {

            Long idLong =  proposalService.updateProposal(proposalId, proposal1, status, customersArr);

            proposalCustomerService.deleteAllWithProposalId(idLong);

            proposalCustomerService.createProposalCustomerArray(customersArr, proposalService.findById(idLong));
            if (json.has("featureList")) {
                JSONArray featureList = json.getJSONArray("featureList");
                Gson gson = new Gson();
                Features features[] = gson.fromJson(featureList.toString(), Features[].class);
                List<Features> list = featureService.saveFromJson(features, idLong);
//          Proposals proposals = proposalService.findById(id);
//          proposals.addFeature(list);
//          proposalService.saveProposal(proposals);
            }
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /*@PutMapping("/proposals")
    public @ResponseBody ResponseEntity updateProposal(@RequestBody(required = false) String jsonBody) {
        JSONObject json = new JSONObject(jsonBody);
        JSONObject proposal1 = json.getJSONObject("proposal");
        String status = json.getString("newStatus");
        Long proposalId = Long.parseLong(json.getJSONObject("id").toString());
        try {
            proposalService.updateProposal(proposalId, proposal1, status);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }*/
    /*@PostMapping("/changeStatus/{id}")
    public @ResponseBody ResponseEntity changeStatus(@PathVariable String id, @RequestBody String proposal) {
        Long proposalId = Long.parseLong(id);
        try {
            proposalService.updateProposal(proposalId, proposal);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }*/

    @PostMapping("/validateProposal")
    public @ResponseBody ResponseEntity validateProposal(@RequestBody(required = false) String jsproposal) {

        Gson g = new Gson();
//        JSONArray customersArr = jsproposal.getJSONArray("customers");
//        JSONObject customerJ;
        Proposals proposal = g.fromJson(jsproposal, Proposals.class);

        if (proposal.getTitle() == null || proposal.getDescription() == null ||
                proposal.getTitle().isEmpty() || proposal.getDescription().isEmpty()) {
            return new ResponseEntity(HttpStatus.CONFLICT);
        }

        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/proposals")
    public @ResponseBody Proposals createProposal(@RequestBody(required = false) String proposal) throws Exception {
        JSONObject json = new JSONObject(proposal);
        JSONObject proposal1 = json.getJSONObject("proposal");
        String oldStatus = json.getString("oldStatus");
        String newStatus = json.getString("newStatus");
        JSONArray customers = json.getJSONArray("customers");
        Long id = proposalService.createProposal(proposal1, oldStatus, newStatus); //customer
        proposalCustomerService.createProposalCustomerArray(customers, proposalService.findById(id));
        if (json.has("featureList")) {
            JSONArray featureList = json.getJSONArray("featureList");
            Gson gson = new Gson();
            Features features[] = gson.fromJson(featureList.toString(), Features[].class);
            List<Features> list = featureService.saveFromJson(features, id);
//          Proposals proposals = proposalService.findById(id);
//          proposals.addFeature(list);
//          proposalService.saveProposal(proposals);
        }
        return proposalService.findById(id);
    }

   /* @GetMapping("/proposals")
    public List<Proposals> getAllProposals() {
        return proposalService.getAll();
    }*/

    /*@GetMapping("/proposalByUserId")
    public List<Proposals> getAllByUserId(String userId){
        return proposalService.getAllByUserId(userId);
       }

    @GetMapping("/userLogin")
    public String getUserLogin(HttpServletRequest request) {
        String login = request.getUserPrincipal().getName();
        return login;
    }*/

    @GetMapping("/getProposals")
    public List<Proposals> getProposalsForUser(HttpServletRequest request) {

            String login = request.getUserPrincipal().getName();
            String user_id = usersService.getUserByLogin(login).getId().toString();
            Long idLong = Long.parseLong(user_id, 10 );
            String rName= usersService.getUserRoleById(idLong);

           if ( (rName.equals("admin")) || (rName.equals("budgetOwner")) ){
               return proposalService.getAll();
           }else{
               return proposalService.getByUser_id(user_id);
           }

    }

    public boolean isNumeric(String s) {
        return s != null && s.matches("[-+]?\\d*\\.?\\d+");
    }
    @GetMapping("/proposals/{id}")
    public Proposals getProposal(@PathVariable String id){
        if (!isNumeric(id)) {
            return null;
        }
        Long proposalId = Long.parseLong(id);
        return proposalService.findById(proposalId);
    }

    @GetMapping("/statuses")
    public List<Statuses> getAllStatuses() {
        return proposalService.getAllStatuses();
    }
}