package edu.proposalbooster.mvc.EmailSender;

import edu.proposalbooster.database.service.EmailSender.EmailSenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.mail.internet.MimeMessage;

@Controller
public class EmailSender {
    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private EmailSenderService emailSenderService;

    @RequestMapping("/sendemail")
    @ResponseBody
    String home(@RequestParam("email") String email, @RequestParam("message") String message) {
        try {
            sendEmail(email, message, "cause we can");
            return "Sucessfully send!!!";
        } catch (Exception e) {
            return "Error in sending" + e;
        }
    }

    private void sendEmail(String email, String message, String subject) throws Exception {
        emailSenderService.sendEmail(email, message, subject);
    }
}
