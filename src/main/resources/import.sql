INSERT INTO public.users (user_id, firstName, lastName, login, password, role, email) VALUES (0, 'admin',  'admin',    'admin',           '$2a$04$dotOzdY4mOgUclGf/CJql.bpA3WORpEbpE3lxG8Ew/DJr9m7QHPqG', 'admin',  'netcracker.proposal@yandex.ru');
INSERT INTO public.users (user_id, firstName, lastName, login, password, role, email) VALUES (1, 'Ruslan', 'Bashirov', 'Ruslan.Bashirov', '$2a$10$sjT6ic/OvTjBn74yYe7q8.eWFzdQFteprHkpIQauD38QpAaXsY3Be', 'po', 'netcracker.proposal@yandex.ru');
INSERT INTO public.users (user_id, firstName, lastName, login, password, role, email) VALUES (2, 'budget', 'owner',    'BO',              '$2a$10$Co7HmcvU4kgy2SMLH/irRu73n4tvV1wBjRPwY.xFqMONzSVJbUeCG', 'budgetOwner', 'netcracker.proposal@yandex.ru');

INSERT INTO public.statuses (status_id, description, name) VALUES
  (0, 'Initiated', 'Initiated'),
  (1, 'In Work', 'In Work'),
  (2, 'In Queue', 'In Queue'),
  (3, 'Approved', 'Approved'),
  (4, 'Archived', 'Archived');

INSERT INTO public.proposals (proposal_id, additional_fields, description, end_date, equipment, expected_profit, man_days, start_date, title, status, user_id, reject_message) VALUES
 (nextval('hibernate_sequence'), null, 'description1', null, 'equipment1', 0, 111, null, 'Billing module', 1, 1, ''),
 (nextval('hibernate_sequence'), null, 'description2', null, 'equipment2', 0, 111, null, 'Salary notifier', 2, 1, ''),
 (nextval('hibernate_sequence'), null, 'description3', null, 'equipment3', 0, 111, null, 'Smart Collector', 2, 1, ''),
 (nextval('hibernate_sequence'), null, 'description4', null, 'equipment4', 0, 111, null, 'Mobile Virtual Network Operator (Implementation project)', 3, 1, ''),
 (nextval('hibernate_sequence'), null, 'description5', null, 'equipment5', 0, 111, null, 'Software development tracker', 3, 1, ''),
 (nextval('hibernate_sequence'), null, 'description6', null, 'equipment6', 0, 111, null, 'University building access system', 3, 1, ''),
 (nextval('hibernate_sequence'), null, 'description7', null, 'equipment7', 0, 111, null, 'Analysis testing system', 4, 1, ''),
 (nextval('hibernate_sequence'), null, 'description8', null, 'equipment8', 0, 111, null, 'Network management system', 3, 1, '');

INSERT into customers VALUES
(nextval('hibernate_sequence'), 'Product Management Team'),
(nextval('hibernate_sequence'), 'NC Edu Center'),
(nextval('hibernate_sequence'), 'Budget Owner Team');

INSERT INTO public.status_changes (change_id, status1Id, status2Id) VALUES
(0, 0, 1),
(1, 1, 4),
(2, 1, 2),
(3, 2, 1),
(4, 2, 3),
(5, 1, 1),
(6, 3, 4);

